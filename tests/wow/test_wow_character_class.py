import unittest
from unittest import TestCase

from flaskwowapp.wow.character_class import get

unittest.TestLoader.testMethodPrefix = 'the_wow_character'


class TestWowCharacterClass(TestCase):

    def the_wow_character_class_should_show_druid_icon(self):
        icon = get('druid').icon

        self.assertEqual((36, 36), icon.size)
        # icon.show()
