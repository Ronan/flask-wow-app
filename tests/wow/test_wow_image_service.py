import os
import unittest
from unittest import TestCase

from flaskwowapp import Database, Filesystem, ApiService, WowDataService, WowImageService, config
from tests import get_project_root

unittest.TestLoader.testMethodPrefix = 'the_service'


class TestWowImageService(TestCase):

    @classmethod
    def setUpClass(cls):
        database = Database()
        filesystem = Filesystem(os.path.join(get_project_root(), 'instance'))
        api = ApiService(config.cfg['wow']['client-id'], config.cfg['wow']['client-secret'])
        wow_data_service = WowDataService(database, filesystem, api)
        cls.service = WowImageService(filesystem, wow_data_service)

    def the_service_should_retrieve_the_avatar_image(self):
        image = self.service.get_character_avatar('eu', 'Sargeras', 'Pamynx')
        self.assertEqual('image/png', image.mimetype)

    def the_service_should_retrieve_the_inset_image(self):
        image = self.service.get_character_inset('eu', 'Sargeras', 'Pamynx')
        self.assertEqual('image/png', image.mimetype)

    def the_service_should_retrieve_the_main_image(self):
        image = self.service.get_character_main('eu', 'Sargeras', 'Pamynx')
        self.assertEqual('image/png', image.mimetype)

    def the_service_should_retrieve_the_player_avatars_image(self):
        image = self.service.get_player_avatars('eu', 'Sargeras', 'Pamynx')
        self.assertEqual('image/png', image.mimetype)

    def the_service_should_retrieve_the_player_insets_image(self):
        image = self.service.get_player_insets('eu', 'Sargeras', 'Pamynx')
        self.assertEqual('image/png', image.mimetype)

    def the_service_should_retrieve_the_guild_avatars_image(self):
        image = self.service.get_guild_avatars('eu', 'Sargeras', 'Pamynx')
        self.assertEqual('image/png', image.mimetype)

