import os
import unittest
from unittest import TestCase

from flaskwowapp import Database, Filesystem, ApiService, WowDataService, WowImageService, config, Character
from tests import get_project_root

unittest.TestLoader.testMethodPrefix = 'the_service'


class TestWowImageService(TestCase):

    @classmethod
    def setUpClass(cls):
        database = Database()
        filesystem = Filesystem(os.path.join(get_project_root(), 'instance'))
        api = ApiService(config.cfg['wow']['client-id'], config.cfg['wow']['client-secret'])
        cls.service = WowDataService(database, filesystem, api)

    def the_service_should_retrieve_a_character(self):
        character = self.service.get_character('eu', 'Sargeras', 'Pamynx')
        self.assertEqual('Pamynx', character.name)
        self.assertGreater(character.achievement_points, 0)
        self.assertGreater(character.heart_azerite_level, 30)

    def the_service_should_not_retrieve_a_non_existing_character(self):
        character = self.service.get_character('eu', 'Sargeras', 'Paminx')
        self.assertIsNone(character)

    def the_service_should_retrieve_a_player_characters(self):
        player = self.service.get_player_for_character('eu', 'Sargeras', 'Pamynx')
        self.assertGreater(len(player.characters), 1)

    def the_service_should_retrieve_a_player_characters_without_duplicates(self):
        player = self.service.get_player_for_character('eu', 'Sargeras', 'Pamynx')

        def key(c: Character):
            return c.region + '-' + c.realm + '-' + c.name

        for character in player.characters:
            character_count = len([
                    key(current)
                    for current in player.characters
                    if key(current) == key(character)
                ])
            self.assertEqual(character_count, 1)

    def the_service_should_sort_characters_of_a_player(self):
        player = self.service.get_player_for_character('eu', 'Sargeras', 'Pamynx')

        for i in range(len(player.characters) - 2):
            self.assertGreaterEqual(
                player.characters[i],
                player.characters[i+1]
            )

    def the_service_should_retrieve_the_guildmates_for_a_characters(self):
        members = self.service.get_guildmates_for_character('eu', 'Sargeras', 'Pamynx', 3)
        self.assertGreater(len(members), 1)
