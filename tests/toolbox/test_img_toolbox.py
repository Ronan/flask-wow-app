import os
import unittest
from unittest import TestCase

from PIL import Image

from flaskwowapp.toolbox.img_toolbox import split_horizontally, split_vertically, split_in_rectangles

unittest.TestLoader.testMethodPrefix = 'the_toolbox'


class TestImgToolbox(TestCase):

    def the_toolbox_should_split_an_image_horizontally(self):
        path = os.path.join(os.path.dirname(__file__), 'low-def.bmp')
        low_def = Image.open(path)
        (width, height) = low_def.size

        images = split_horizontally(low_def, 2)

        self.assertEqual(width, images[0].size[0])
        self.assertEqual(width, images[1].size[0])
        self.assertEqual(height, images[0].size[1] + images[1].size[1])
        # images[0].show()
        # images[1].show()

    def the_toolbox_should_split_an_image_vertically(self):
        path = os.path.join(os.path.dirname(__file__), 'low-def.bmp')
        low_def = Image.open(path)
        (width, height) = low_def.size

        images = split_vertically(low_def, 6)

        for index in range(6):
            self.assertEqual(height, images[index].size[1])

        self.assertEqual(
            width,
            sum([image.size[0] for image in images])
        )
        # images[0].show()

    def the_toolbox_should_split_an_image(self):
        path = os.path.join(os.path.dirname(__file__), 'low-def.bmp')
        low_def = Image.open(path)
        (width, height) = low_def.size

        box = split_in_rectangles(low_def, 6, 2)[0][0]

        self.assertEqual((width / 6, height / 2), box.size)
        # box.show()

