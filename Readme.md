Commands
---
For Linux and Mac:

    export FLASK_APP=flaskwowapp
    export FLASK_ENV=development
    flask run

For Windows cmd, use set instead of export:

    set FLASK_APP=flaskwowapp
    set FLASK_ENV=development
    flask run

Initialize a DB:
    
    flask init-db
    
Run unit tests

    python -m unittest discover -v
    
TODO List
---
- Use sqlite database
- Automatically refresh data
- Deploy