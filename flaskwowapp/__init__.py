import json
import os

from flask import Flask, send_file, Response

from flaskwowapp import config
from flaskwowapp.wow.data.image_response import ImageResponse
from flaskwowapp.wow.api_service import ApiService
from flaskwowapp.wow.data.character import Character
from flaskwowapp.wow.database import Database
from flaskwowapp.wow.filesystem import Filesystem
from flaskwowapp.wow.data.player import Player
from flaskwowapp.wow.wow_data_service import WowDataService
from flaskwowapp.wow.wow_image_service import WowImageService


def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY=config.cfg['flask']['secret'],
        DATABASE=os.path.join(app.instance_path, 'flaskwowapp.sqlite'),
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    database = Database()
    filesystem = Filesystem(app.instance_path)
    api = ApiService(config.cfg['wow']['client-id'], config.cfg['wow']['client-secret'])
    wow_data_service = WowDataService(database, filesystem, api)
    wow_image_service = WowImageService(filesystem, wow_data_service)

    @app.route('/avatar/<region>/<realm>/<name>.png')
    def avatar(region, realm, name):
        img: ImageResponse = wow_image_service.get_character_avatar(region, realm, name)
        return send_file(img.path, mimetype=img.mimetype)

    @app.route('/inset/<region>/<realm>/<name>.png')
    def inset(region, realm, name):
        img: ImageResponse = wow_image_service.get_character_inset(region, realm, name)
        return send_file(img.path, mimetype=img.mimetype)

    @app.route('/main/<region>/<realm>/<name>.png')
    def main(region, realm, name):
        img: ImageResponse = wow_image_service.get_character_main(region, realm, name)
        return send_file(img.path, mimetype=img.mimetype)

    @app.route('/player-avatars/<region>/<realm>/<name>.png')
    def player_avatars(region, realm, name):
        img: ImageResponse = wow_image_service.get_player_avatars(region, realm, name)
        return send_file(img.path, mimetype=img.mimetype)

    @app.route('/player-insets/<region>/<realm>/<name>.png')
    def player_insets(region, realm, name):
        img: ImageResponse = wow_image_service.get_player_insets(region, realm, name)
        return send_file(img.path, mimetype=img.mimetype)

    @app.route('/guild-avatars/<region>/<realm>/<name>.png')
    def guild_avatars(region, realm, name):
        img: ImageResponse = wow_image_service.get_guild_avatars(region, realm, name)
        return send_file(img.path, mimetype=img.mimetype)

    @app.route('/player/<region>/<realm>/<name>.json')
    def player(region, realm, name):
        player: Player = wow_data_service.get_player_for_character(region, realm, name)
        return Response(json.dumps(player.to_dict()), mimetype='application/json')

    # Not used
    from . import db
    db.init_app(app)

    # Not used
    from . import auth
    app.register_blueprint(auth.bp)

    return app
