import os

import yaml

with open(os.path.join(os.path.dirname(__file__), 'config.yml'), 'r') as ymlfile:
    cfg = yaml.load(ymlfile, Loader=yaml.FullLoader)
