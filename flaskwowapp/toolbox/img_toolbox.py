from PIL import Image, ImageDraw


def concat_right(image1: Image, image2: Image) -> Image:
    result = Image.new(
        'RGB',
        (
            image1.size[0] + image2.size[0],
            max(image1.size[1], image2.size[1])
        )
    )
    result.paste(image1, (0, 0, image1.size[0], image1.size[1]))
    result.paste(image2, (image1.size[0], 0, image1.size[0] + image2.size[0], image2.size[1]))
    return result


def concat_down(image1: Image, image2: Image) -> Image:
    result = Image.new(
        'RGB',
        (
            max(image1.size[0], image2.size[0]),
            image1.size[1] + image2.size[1]
        )
    )
    result.paste(image1, (0, 0, image1.size[0], image1.size[1]))
    result.paste(image2, (0, image1.size[1], image2.size[0], image1.size[1] + image2.size[1]))
    return result


def to_circle(image: Image) -> Image:
    big_size = (image.size[0] * 3, image.size[1] * 3)
    mask: Image = Image.new('L', big_size, 0)
    draw: ImageDraw = ImageDraw.Draw(mask)
    draw.ellipse((0, 0) + big_size, fill=255)
    mask = mask.resize(image.size, Image.ANTIALIAS)
    image.putalpha(mask)
    return image


def split_vertically(source: Image, parts: int) -> [Image]:
    (width, height) = source.size
    result = []
    for index in range(parts):
        box = (index * width / parts, 0, (index + 1) * width / parts, height)
        result.append(source.crop(box))
    return result


def split_horizontally(source: Image, parts: int) -> [Image]:
    (width, height) = source.size
    result = []
    for index in range(parts):
        box = (0, index * height / parts, width, (index + 1) * height / parts)
        result.append(source.crop(box))
    return result


def split_in_rectangles(source: Image, number_of_columns: int, number_of_rows: int) -> [[Image]]:
    return [
        split_horizontally(current, number_of_rows)
        for current in (split_vertically(source, number_of_columns))
    ]
