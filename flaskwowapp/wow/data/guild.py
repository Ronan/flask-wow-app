class Guild:
    def __init__(self, region, realm, name) -> None:
        self.id = None
        self.region = region
        self.realm = realm
        self.name = name
