from flaskwowapp import Character


class Player:
    def __init__(self, points) -> None:
        self.points = points
        self.characters: list[Character] = []

    def to_dict(self):
        return {
            'points': self.points,
            'characters': [
                character.to_dict()
                for character in self.characters
            ]
        }
