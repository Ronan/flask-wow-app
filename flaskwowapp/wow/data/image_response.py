class ImageResponse:
    def __init__(self, path, mimetype) -> None:
        self.path = path
        self.mimetype = mimetype
