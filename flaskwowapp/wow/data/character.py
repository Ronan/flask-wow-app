class Character:
    def __init__(self, region, realm, name) -> None:
        self.id = None
        self.guild_id = None
        self.region = region
        self.realm = realm
        self.name = name
        self.achievement_points = 0
        self.thumbnail = None
        self.character_class = None
        self.level = 0
        self.average_item_level = 0
        self.average_item_level_equipped = 0
        self.heart_item_level = 0
        self.heart_azerite_level = 0
        self.guild_rank = 0

    def __gt__(self, other):
        if self.level == other.level:
            return self.average_item_level > other.average_item_level
        else:
            return self.level > other.level

    def __ge__(self, other):
        if self.level == other.level:
            return self.average_item_level >= other.average_item_level
        else:
            return self.level > other.level

    def to_dict(self) -> dict:
        return {
            'region': self.region,
            'realm': self.realm,
            'name': self.name,
            'achievement_points': self.achievement_points,
            'thumbnail': self.thumbnail
        }
