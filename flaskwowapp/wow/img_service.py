import os

from PIL import Image, ImageDraw, ImageFont

from flaskwowapp import Character
from flaskwowapp.wow import character_class


def transform_character_avatar(avatar: Image, character: Character) -> Image:
    result = Image.new('RGBA', (100, 100), 0)
    avatar = avatar.resize((100, 100), Image.ANTIALIAS)
    result.paste(avatar, mask=get_circular_mask((100, 100), 87))

    class_icon = character_class.get(character.character_class).icon
    size = 22
    class_icon = class_icon.resize((size, size), Image.ANTIALIAS)
    result.paste(class_icon, (int(78-size/2), int(78-size/2)))

    texture = __get_avatar_texture()
    texture = texture.crop((141, 4, 220, 83))
    texture = texture.resize((100, 100), Image.ANTIALIAS)
    result.paste(texture, mask=texture)

    final = Image.new('RGBA', (115, 115), 0)
    final.paste(result, (15, 15), mask=get_texture_mask())

    draw: ImageDraw = ImageDraw.Draw(final)
    font: ImageFont = ImageFont.truetype(os.path.join(os.path.dirname(__file__), 'fonts', 'Roboto-Black.ttf'), 15)
    draw.text((0, 0), character.name, character_class.get(character.character_class).color, font=font)

    if character.heart_azerite_level > 0:
        heart_circle = get_level_ring(character.heart_azerite_level, texture)
        final.paste(
            heart_circle,
            (5, 50),
            get_circular_mask(heart_circle.size, 100)
        )

    level = character.level
    if level == 120:
        level = character.average_item_level

    level_circle = get_level_ring(level, texture)
    final.paste(
        level_circle,
        (5, 20),
        get_circular_mask(level_circle.size, 100)
    )

    return final


def get_level_ring(level, texture):
    size = 34
    mini_circle = Image.new('RGB', (size, size), 0)

    center = (80, 79)
    mini_circle.paste(texture.crop(
        (
            int(center[0] - size / 2), int(center[1] - size / 2),
            int(center[0] + size / 2), int(center[1] + size / 2)
        )
    ))

    draw: ImageDraw = ImageDraw.Draw(mini_circle)
    ray = 10
    draw.ellipse([(16 - ray, 16 - ray), (16 + ray, 16 + ray)], fill=(0, 0, 0))

    font: ImageFont = ImageFont.truetype(os.path.join(os.path.dirname(__file__), 'fonts', 'Roboto-Black.ttf'), 12)
    text_size = font.getsize(str(level))
    draw.text((int(size / 2 - text_size[0] / 2), 8), str(level), (255, 255, 255), font=font)

    return mini_circle


def get_circular_mask(size, ray):
    mask = Image.new('L', (1000, 1000), 0)
    draw = ImageDraw.Draw(mask)
    draw.ellipse([(500 - ray*5, 500 - ray*5), (500 + ray*5, 500 + ray*5)], fill=255)
    mask = mask.resize((size[0], size[1]), Image.ANTIALIAS)
    return mask


def get_texture_mask():
    mask = get_circular_mask((100, 100), 87)
    draw = ImageDraw.Draw(mask)
    draw.rectangle([(50, 50), (100, 100)], fill=255)
    return mask


def __get_avatar_texture() -> Image:
    png_path = os.path.join(os.path.dirname(__file__), 'textures', 'avatar.png')
    return Image.open(png_path)
