from functools import reduce
from io import BytesIO
from typing import Optional

import requests
from PIL import Image
from requests import Response

from flaskwowapp import ImageResponse, Character, Filesystem
from flaskwowapp.wow.img_service import transform_character_avatar
from flaskwowapp.toolbox.img_toolbox import concat_right, concat_down
from flaskwowapp.wow.wow_data_service import WowDataService


class WowImageService:
    def __init__(self, filesystem: Filesystem, wow_data_service: WowDataService):
        self.filesystem = filesystem
        self.wow_data_service = wow_data_service

    def get_character_avatar(self, region, realm, name) -> Optional[ImageResponse]:
        return self.__get_character_image(region, realm, name, 'avatar', transform_character_avatar)

    def get_player_avatars(self, region, realm, name) -> Optional[ImageResponse]:
        image = self.filesystem.get_player_image(region, realm, name, 'avatar')
        if image is None:
            player = self.wow_data_service.get_player_for_character(region, realm, name)
            resulting_image = self.__aggregate_player_avatars(player)
            self.filesystem.set_player_image(region, realm, name, 'avatar', resulting_image)
            image = self.filesystem.get_player_image(region, realm, name, 'avatar')
        return image

    def get_guild_avatars(self, region, realm, name, max_rank=2) -> Optional[ImageResponse]:
        image = self.filesystem.get_guild_image(region, realm, name)
        if image is None:
            players = self.wow_data_service.get_guildmates_for_character(region, realm, name, max_rank)
            image = reduce(concat_down, [
                self.__aggregate_player_avatars(player)
                for player in players
            ])
            self.filesystem.set_guild_image(region, realm, name, image)
            image = self.filesystem.get_guild_image(region, realm, name)
        return image

    def get_character_inset(self, region, realm, name) -> Optional[ImageResponse]:
        return self.__get_character_image(region, realm, name, 'inset')

    def get_player_insets(self, region, realm, name) -> Optional[ImageResponse]:
        image = self.filesystem.get_player_image(region, realm, name, 'inset')
        if image is None:
            player = self.wow_data_service.get_player_for_character(region, realm, name)
            resulting_image = self.__aggregate_player_insets(player)
            self.filesystem.set_player_image(region, realm, name, 'inset', resulting_image)
            image = self.filesystem.get_player_image(region, realm, name, 'inset')
        return image

    def get_character_main(self, region, realm, name) -> Optional[ImageResponse]:
        return self.__get_character_image(region, realm, name, 'main')

    def __get_character_image(self, region, realm, name, layout, transformation=None) -> Optional[ImageResponse]:
        image: ImageResponse = self.filesystem.get_character_image(region, realm, name, layout)
        if image is None:
            character: Character = self.wow_data_service.get_character(region, realm, name)
            response = self.__get_rendered_image(region, character.thumbnail, layout)
            if response.status_code == 404:
                return None
            img: Image = Image.open(BytesIO(response.content))
            if transformation is not None:
                img = transformation(img, character)
            self.filesystem.save_character_image(region, realm, name, layout, img)
            image = self.filesystem.get_character_image(region, realm, name, layout)
        return image

    def __aggregate_player_avatars(self, player) -> Image:
        return self.__aggregate_player_images(player, 'avatar', concat_right, transform_character_avatar)

    def __aggregate_player_insets(self, player) -> Image:
        return self.__aggregate_player_images(player, 'inset', concat_down)

    def __aggregate_player_images(self, player, layout, concat, transformation=None):
        return reduce(
            concat,
            map(
                lambda image: Image.open(image.path),
                filter(
                    lambda image: image is not None,
                    map(
                        lambda character:
                            self.__get_character_image(
                                character.region, character.realm, character.name,
                                layout, transformation
                            ),
                        player.characters
                    )
                )
            )
        )

    @staticmethod
    def __get_rendered_image(region, thumbnail, layout) -> Response:
        url = 'http://render-' + region + '.worldofwarcraft.com/character/' + thumbnail.replace('avatar', layout)
        return requests.get(url, stream=True)

