import json
import os
from typing import Optional

from PIL import Image

from flaskwowapp import ImageResponse


class Filesystem:
    def __init__(self, root_directory) -> None:
        self.root_directory = root_directory

    def get_character_data(self, region, realm, name) -> Optional[dict]:
        data = None
        data_file = self.get_data_file('characters', region, realm, name)
        if os.path.isfile(data_file):
            with open(data_file) as json_file:
                data = json.load(json_file)
        return data

    def set_character_data(self, region, realm, name, data) -> None:
        data_file = self.get_data_file('characters', region, realm, name)
        with open(data_file, 'w') as data_file:
            json.dump(data, data_file)

    def get_guild_data(self, region, realm, name) -> Optional[dict]:
        data = None
        data_file = self.get_data_file('guilds', region, realm, name)
        if os.path.isfile(data_file):
            with open(data_file) as json_file:
                data = json.load(json_file)
        return data

    def set_guild_data(self, region, realm, name, data) -> None:
        data_file = self.get_data_file('guilds', region, realm, name)
        with open(data_file, 'w') as data_file:
            json.dump(data, data_file)

    def get_data_file(self, data_type: str, region, realm, name):
        return os.path.join(self.get_data_dir(data_type, region, realm, name), 'data.json')

    def get_data_dir(self, data_type: str, region, realm, name):
        data_dir = os.path.join(self.root_directory, 'wow-api', data_type, region, realm, name)
        if not os.path.exists(data_dir):
            os.makedirs(data_dir)
        return data_dir

    def get_character_image(self, region, realm, name, layout: str) -> Optional[ImageResponse]:
        image_file = self.get_image_file(region, realm, name, layout)
        if os.path.isfile(image_file):
            return ImageResponse(image_file, 'image/png')
        else:
            return None

    def set_character_image(self, region, realm, name, layout: str, response):
        image_file = self.get_image_file(region, realm, name, layout)
        with open(image_file, "wb") as target:
            for chunk in response.iter_content(1024):
                target.write(chunk)

    def save_character_image(self, region, realm, name, layout, image: Image):
        image_file = self.get_image_file(region, realm, name, layout)
        image.save(image_file, 'PNG')

    def get_player_image(self, region, realm, name, layout) -> Optional[ImageResponse]:
        image_file = self.get_image_file(region, realm, name, layout + '-player')
        if os.path.isfile(image_file):
            return ImageResponse(image_file, 'image/png')
        else:
            return None

    def set_player_image(self, region, realm, name, layout, image: Image):
        image_file = self.get_image_file(region, realm, name, layout + '-player')
        image.save(image_file, 'PNG')

    def set_guild_image(self, region, realm, name, image):
        image_file = self.get_image_file(region, realm, name, 'guild')
        image.save(image_file, 'PNG')

    def get_guild_image(self, region, realm, name):
        image_file = self.get_image_file(region, realm, name, 'guild')
        if os.path.isfile(image_file):
            return ImageResponse(image_file, 'image/png')
        else:
            return None

    def get_image_file(self, region, realm, name, layout: str):
        return os.path.join(self.get_data_dir('characters', region, realm, name), layout + '.png')

