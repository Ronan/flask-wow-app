from typing import Optional

import requests
from wowapi import WowApi, WowApiException


class ApiService:
    def __init__(self, client_id: str, client_secret: str) -> None:
        self.api = WowApi(client_id, client_secret)

    def get_character_data(self, region, realm, name) -> Optional[dict]:
        try:
            return self.api.get_character_profile(region, realm, name,
                                                  fields='guild,items')
        except WowApiException:
            return None

    def get_guild_data(self, region, realm, name):
        try:
            return self.api.get_guild_profile(region, realm, name,
                                              fields='members')
        except WowApiException:
            return None

    def get_rendered_image(self, region, thumbnail, layout):
        url = 'http://render-' + region + '.worldofwarcraft.com/character/' \
              + thumbnail.replace('avatar', layout)
        return requests.get(url, stream=True)
