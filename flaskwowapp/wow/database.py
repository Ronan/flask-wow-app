import uuid
from typing import Optional, List

from flaskwowapp import Character
from flaskwowapp.wow.data.guild import Guild


class Database:
    def __init__(self) -> None:
        self.characters = {}
        self.guilds = {}

    def find_character(self, region, realm, name) -> Optional[Character]:
        return next(
            (
                character
                for character in self.characters.values()
                if character.region == region and character.realm == realm and character.name == name
            ),
            None
        )

    def find_characters_by_guild_id(self, guild_id) -> List[Character]:
        return [
            character
            for character in self.characters.values()
            if character.guild_id == guild_id
        ]

    def set_character(self, character) -> None:
        existing = self.find_character(character.region, character.realm, character.name)
        if existing is None:
            character.id = str(uuid.uuid4())
        else:
            character.id = existing.id
        self.characters[character.id] = character

    def find_guild(self, region, realm, name) -> Guild:
        return next(
            (
                guild
                for guild in self.guilds.values()
                if guild.region == region and guild.realm == realm and guild.name == name
            ),
            None
        )

    def set_guild(self, guild) -> None:
        existing = self.find_guild(guild.region, guild.realm, guild.name)
        if existing is None:
            guild.id = str(uuid.uuid4())
        else:
            guild.id = existing.id
        self.guilds[guild.id] = guild

