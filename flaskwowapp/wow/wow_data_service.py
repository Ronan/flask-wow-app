from typing import Optional

from flaskwowapp import Character, Database, Filesystem, ApiService, Player
from flaskwowapp.wow.data.guild import Guild


class WowDataService:

    def __init__(self, database: Database, filesystem: Filesystem, api: ApiService):
        self.database = database
        self.filesystem = filesystem
        self.api = api

    def get_character(self, region, realm, name, guild_rank=None) -> Character:
        character = self.database.find_character(region, realm, name)
        if character is None:
            data = self.filesystem.get_character_data(region, realm, name)
            if data is None:
                data = self.api.get_character_data(region, realm, name)
                self.filesystem.set_character_data(region, realm, name, data)
            if data is not None:
                character = self.__to_character(region, data)
                guild = self.get_guild(region, data['guild']['realm'], data['guild']['name'])
                character.guild_id = guild.id
                if guild_rank is not None:
                    character.guild_rank = guild_rank
                self.database.set_character(character)
        return character

    def get_player_for_character(self, region, realm, name) -> Player:
        character: Character = self.get_character(region, realm, name)
        members = self.database.find_characters_by_guild_id(character.guild_id)
        player: Player = Player(character.achievement_points)
        player.characters = sorted([
            member
            for member in members
            if member.achievement_points == character.achievement_points
        ], reverse=True)
        return player

    def get_guildmates_for_character(self, region, realm, name, max_rank=None):
        character: Character = self.get_character(region, realm, name)
        members = sorted(
            self.database.find_characters_by_guild_id(character.guild_id),
            reverse=True
        )
        members = filter(lambda member: max_rank is None or max_rank >= member.guild_rank, members)
        result = self.uniq([
            self.get_player_for_character(character.region, character.realm, character.name)
            for character in members
        ], lambda player: player.points)
        return result

    def get_guild(self, region, realm, name) -> Guild:
        guild = self.database.find_guild(region, realm, name)
        if guild is None:
            data = self.filesystem.get_guild_data(region, realm, name)
            if data is None:
                data = self.api.get_guild_data(region, realm, name)
                self.filesystem.set_guild_data(region, realm, name, data)
            guild = self.__to_guild(region, data)
            self.database.set_guild(guild)
            if data is not None:
                for member in data['members']:
                    self.get_character(region, member['character']['realm'], member['character']['name'], member['rank'])
        return guild

    def __to_character(self, region, data: dict) -> Optional[Character]:
        if data is None:
            return None
        else:
            character = Character(region, data['realm'], data['name'])
            character.achievement_points = data['achievementPoints']
            character.thumbnail = data['thumbnail']
            character.character_class = self.__to_class_name(data['class'])
            character.level = data['level']
            character.average_item_level = data['items']['averageItemLevel']
            character.average_item_level_equipped = data['items']['averageItemLevelEquipped']
            if 'neck' in data['items'].keys() and data['items']['neck']['quality'] == 6:
                character.heart_item_level = data['items']['neck']['itemLevel']
                character.heart_azerite_level = data['items']['neck']['azeriteItem']['azeriteLevel']
            else:
                character.heart_item_level = 0
                character.heart_azerite_level = 0
            return character

    @staticmethod
    def __to_class_name(class_index: int) -> str:
        classes = {
            1: 'warrior',
            2: 'paladin',
            3: 'hunter',
            4: 'rogue',
            5: 'priest',
            6: 'death-knight',
            7: 'shaman',
            8: 'mage',
            9: 'warlock',
            10: 'monk',
            11: 'druid',
            12: 'demon-hunter'
        }
        return classes[class_index]

    @staticmethod
    def __to_guild(region, data: dict) -> Optional[Guild]:
        if data is None:
            return None
        else:
            guild = Guild(region, data['realm'], data['name'])
            return guild

    @staticmethod
    def uniq(seq, key=None):
        if key is None:
            def key(x): return x
        seen = {}
        result = []
        for item in seq:
            marker = key(item)
            if marker in seen:
                continue
            seen[marker] = 1
            result.append(item)
        return result
