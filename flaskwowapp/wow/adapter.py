from typing import Optional

from flaskwowapp import Character
from flaskwowapp.wow.data.guild import Guild


def to_character(region, data: dict) -> Optional[Character]:
    if data is None:
        return None
    else:
        character = Character(region, data['realm'], data['name'])
        character.achievement_points = data['achievementPoints']
        character.thumbnail = data['thumbnail']
        character.character_class = to_class_name(data['class'])
        character.level = data['level']
        character.average_item_level = data['items']['averageItemLevel']
        character.average_item_level_equipped = data['items']['averageItemLevelEquipped']
        if 'neck' in data['items'].keys() and data['items']['neck']['quality'] == 6:
            character.heart_item_level = data['items']['neck']['itemLevel']
            character.heart_azerite_level = data['items']['neck']['azeriteItem']['azeriteLevel']
        else:
            character.heart_item_level = 0
            character.heart_azerite_level = 0
        return character


def to_class_name(class_index: int) -> str:
    classes = {
        1: 'warrior',
        2: 'paladin',
        3: 'hunter',
        4: 'rogue',
        5: 'priest',
        6: 'death-knight',
        7: 'shaman',
        8: 'mage',
        9: 'warlock',
        10: 'monk',
        11: 'druid',
        12: 'demon-hunter'
    }
    return classes[class_index]


def to_guild(region, data: dict) -> Optional[Guild]:
    if data is None:
        return None
    else:
        guild = Guild(region, data['realm'], data['name'])
        return guild

