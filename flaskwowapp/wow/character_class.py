import os

from PIL import Image

from flaskwowapp.toolbox.img_toolbox import split_in_rectangles


def get_class_icon(coords) -> Image:
    path = os.path.join(os.path.dirname(__file__), 'icons', 'class', 'low-def.bmp')
    class_icons = Image.open(path)
    return split_in_rectangles(class_icons, 6, 2)[coords[0]][coords[1]]


class CharacterClass:

    def __init__(self, name, color, coords) -> None:
        self.name = name
        self.color = color
        self.coords = coords
        self.icon = get_class_icon(coords)


def get(name: str) -> CharacterClass:
    return all_classes[name]


all_classes = {
    'warrior': CharacterClass('warrior', (199, 156, 110), (4, 1)),
    'paladin': CharacterClass('paladin', (245, 140, 186), (4, 0)),
    'hunter': CharacterClass('hunter', (171, 212, 115), (2, 0)),
    'rogue': CharacterClass('rogue', (255, 245, 105), (1, 1)),
    'priest': CharacterClass('priest', (255, 255, 255), (0, 1)),
    'death-knight': CharacterClass('death-knight', (196, 31, 59), (0, 0)),
    'shaman': CharacterClass('shaman', (0, 112, 222), (2, 1)),
    'mage': CharacterClass('mage', (64, 199, 235), (3, 0)),
    'warlock': CharacterClass('warlock', (135, 135, 237), (3, 1)),
    'monk': CharacterClass('monk', (0, 255, 150), (5, 0)),
    'druid': CharacterClass('druid', (255, 125, 10), (1, 0)),
    'demon-hunter': CharacterClass('demon-hunter', (163, 48, 201), (5, 1)),
}



